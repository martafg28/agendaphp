<?php
// variables
$crear_tabla = 'CREATE TABLE IF NOT EXISTS contactos(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    nombre VARCHAR(40), 
                    apellidos VARCHAR(60), 
                    telefono VARCHAR(10),
                    correo VARCHAR(50)
                    )'; 
                    
//introduciremos unos datos en la tabla contactos   
$datos= array(
              array('nombre'=>"Marta",
                    'apellidos'=>"Lopez",
                    'telefono'=>"976111111",
                    'correo'=>"marta@marta.com"),
                    
              array('nombre'=>"luis",
                    'apellidos'=>"Perez",
                    'telefono'=>"976222222",
                    'correo'=>"luis@luis.com"),
                    
              array('nombre'=>"Jose",
                    'apellidos'=>"Fernandez",
                    'telefono'=>"976333333",
                    'correo'=>"jose@jose.com")
             
        );              
?>        



<?php
// test de acceso a sqlite
$nombre_fichero = 'agenda.db';
if(file_exists($nombre_fichero)){
    echo "<h3>El fichero $nombre_fichero ya existe</h3><br>";
    
}else{

    try{
    // crear bases de datos
        $conn=new PDO('sqlite:agenda.db');
        //$conn=new PDO('sqlite::memory:');  --> crea base de datos en memoria no en el sistema de ficheros
        //echo "Funciona<br>";
        $conn->exec($crear_tabla);
        //insertar datos
        //1. preparar sentencia de insercion
        $insertar="INSERT into contactos(nombre,apellidos, telefono, correo)
                    VALUES(:nombre,:apellidos,:telefono,:correo)";
        $sentencia=$conn->prepare($insertar);
        foreach($datos as $contacto){
                $sentencia->execute($contacto);
            
        }
            
        //buscar datos
        $listado='SELECT * FROM contactos';
        $resultado=$conn->query($listado);
        
        echo "<h3>Datos iniciales insertados</h3><br>";
        foreach($resultado as $cont){
            echo 'id: <strong>', $cont['id'], '</strong><br>';
            echo 'nombre: <strong>', $cont['nombre'], '</strong><br>';
            echo 'apellidos: <strong>', $cont['apellidos'], '</strong><br>';
            echo 'telefono: <strong>', $cont['telefono'], '</strong><br>';
            echo 'correo: <strong>', $cont['correo'], '</strong><br><hr>';
                
        }//foreach
    }catch(PDOException $e){
         echo $e->getMessage();
    
    }//del catch

    //cierra conexion
    $conn=null;
    echo "<h3>El fichero '".$nombre_fichero."' ha sido creado con exito.</h3><br>";   //el punto concatena (si pongo comillas dobles)
    echo '<a href="./index.html">Inicio</a>';
}//del else
?>