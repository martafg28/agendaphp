<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Buscar</title>
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <h1>BUSCAR CONTACTO</h1>

        <div class="contenido">
       '<form action="buscarContacto.php" method="post" class="form-horizontal" >
          <div class="form_group">
            <label for="nombre" class="col-sm-2 control-label">Nombre</label>
            <div class="col-xs-4">
              <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
            </div>
          </div>
    
      <div class="form-group">
        <label for="apellidos" class="col-sm-2 control-label">Apellidos</label>
        <div class="col-xs-4">
          <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos">
        </div>
      </div>
    
      <div class="form-group">
        <label for="telefono" class="col-sm-2 control-label">Telefono</label>
        <div class="col-xs-4">
          <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Telefono">
        </div>
      </div>
    
      <div class="formulario">
        <label for="correo" class="col-sm-2 control-label">Correo</label>
        <div class="col-xs-4">
          <input type="text" class="form-control" id="correo" name="correo" placeholder="Correo Electrónico">
        </div>
      </div>
    
      <div class="formulario">
        <div class="col-sm-offset-2 col-sm-10">
          <input type="submit" class="btn btn-default" value="Buscar">
       </form>  
        </div>
     </body>
  </html>      




