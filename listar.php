<?php
try {
    // crear/conectar a bases de datos
    $conn = new PDO('sqlite:agenda.db');
    
    // select
    $listado = 'SELECT * FROM contactos';
    $resultado = $conn->query($listado);
    
    //para pintar en listado.php
    foreach($resultado as $cont){
        echo 'id: <strong>', $cont['id'], '</strong><br>';
        echo 'nombre: <strong>', $cont['nombre'], '</strong><br>';
        echo 'apellidos: <strong>', $cont['apellidos'], '</strong><br>';
        echo 'telefono: <strong>', $cont['telefono'], '</strong><br>';
        echo 'correo: <strong>', $cont['correo'], '</strong><br>';
        echo '<hr>';
        echo "<a href='./editar.php?id=", $cont['id'], "' > Editar </a> --- ";
        echo "<a href='./eliminar.php?id=", $cont['id'], "' > Eliminar </a>";
        echo '<hr>';
    }
        
} //fin try
catch(PDOException $e){
    echo $e->getMessage();
} //fin catch